/*--------------------------------------------------------------------------*/
// Program	: Module function control LCD display 
// Description 	: Call function control LCD display group type 4 bit  
// Filename	: lcd_robo51.h
// C compiler	: RIDE 51 V6.1
/*--------------------------------------------------------------------------*/
#define line1 0x80	// Define constant
#define line2 0xC0	// Define constant
#define lcd_clear() lcd_command(1)
sbit rs = P0^2;      	// Bit control command for LCD
sbit e = P0^3;		// Bit control pulse command for LCD
enum{Hex,Dec};		// Format display integer
/*****************************************************************************/
/***************** Function delay 1 ms per unit ******************************/
/*****************************************************************************/
void lcd_delay(unsigned int ms)
{
	unsigned int x,a;		// Keep for counter loop  
	for(x=0;x<ms;x++)
	{
		for(a=0;a<878;a++);	// Loop for delay 1 m� per unit
	}
}
/*****************************************************************************/
/***************** Function send command to LCD format 4 Bit *****************/
/*****************************************************************************/
void lcd_command(unsigned char com)	
{
	unsigned char buff;        // For Keep command send to LCD
	buff = com & 0xF0;         // Keep 4 bit high byte before send to LCD 
	rs = 0;			// Select send command to LCD
       	e = 1;			// Start generate pulse clock LCD
	P0 = (P0 & 0x0F)|buff;     // Send data to LCD port
	lcd_delay(1);              // Delay 1 ms
	e = 0;                     // Stop generate pulse clock LCD
	lcd_delay(1);              // Delay 1 ms
	
	buff = (com & 0x0F)<<4;		// Keep 4 bit low byte before send to LCD
	rs = 0;		// Select send command to LCD 
      	e = 1;			//	start generate pulse clock LCD
	P0 = (P0 & 0x0F)|buff ;    // Send data to LCD port
	lcd_delay(1);             	// Delay 1 ms
	e = 0;							// Stop generate pulse clock LCD
	lcd_delay(1);            	// Delay 1 ms
}
/*****************************************************************************/
/*********************** Function send data to LCD format 4 Bit **************/
/*****************************************************************************/
void lcd_text(char text)		
{
	unsigned char buff;       		// For Keep data send to LCD
	buff = text & 0xF0;		// Keep 4 bit high byte before send to LCD
	rs = 1;		// Select send data to LCD       	     		
	e = 1;		//	start generate pulse clock LCD
	P0 = (P0 & 0x0F)|buff ;    // Send data to LCD port
	lcd_delay(1);					// Delay 1 ms
	e = 0;                    	// Stop generate pulse clock LCD
	lcd_delay(1);             	// Delay 1 ms
	
	buff = (text & 0x0F)<<4; 	// Keep 4 bit low byte before send to LCD
	rs = 1;				// Write Command to LCD       	     		
	e = 1;				// start generate pulse clock LCD
	P0 = (P0 & 0x0F)|buff ;    // Send data to LCD port
	lcd_delay(1);					// Delay 1 ms
	e = 0;                    	// Stop generate pulse clock LCD
	lcd_delay(1); 					// Delay 1 ms
}
/*****************************************************************************/
/***************** Function show string message ******************************/
/*****************************************************************************/
void lcd_putstr(unsigned char line,char *p)
{
	lcd_command(0x02);         // Set origin address of LCD
	lcd_command(line);			// Set address 00H of LCD
	while(*p)                  // Check data pointer = 0?
	{
	 lcd_text(*p);             // Send data to LCD
	 p++;                      // Increase address 1 time 
	}
}

/*****************************************************************************/
/***************** Function set initial format LCD 4 Bit ******************/
/*****************************************************************************/
void lcd_init(void)
{
	lcd_delay(500);       	// Delay 500 ms
	lcd_command(0x33);      // Command set format 4 bit 
	lcd_command(0x32);		// Command set format 4 bit
	lcd_command(0x28);		// 2 line ,8 bit display ,5*7 dot
	lcd_command(0x0C);      // Display ON , none cursor
	lcd_command(0x01);		// Clear screen
}
/*****************************************************************************/
/***************** Function set initial parameter LCD 4 Bit ******************/
/*****************************************************************************/
void inttolcd(unsigned char line_sel,int value,int format)
{
	char index=0,buff[6];	// For keep string send to LCD 
	switch(format)          // Select format integer
	{
	case Dec : 	buff[0] = (value/10000) | 0x30;		// Convert data to ASCII 10000'th 
			buff[1] = ((value%10000)/1000) | 0x30;	// Convert data to ascii 1000'th
			buff[2] = (((value%10000)%1000)/100) | 0x30;   	// Convert data to ascii 100'th
			buff[3] = ((((value%10000)%1000)%100)/10) | 0x30;	// Convert data to ascii 10'th
			buff[4] = ((((value%10000)%1000)%100)%10) | 0x30;  // Convert data to ascii 1'th
			buff[5] = 0;	// End of data
			break;  		// Out block for decimal format case
	case Hex : 	buff[0] = (value & 0xF000)>>12;    	// Filter 4 bit 1'th
			if(buff[0]<10){buff[0] |= 0x30;} else {buff[0] = (buff[0]-9) | 0x40;}	// Convert data to ascii 
			buff[1] = (value & 0x0F00)>>8;		// Filter 4 bit 2'th
			if(buff[1]<10){buff[1] |= 0x30;} else {buff[1] = (buff[1]-9) | 0x40;}	// Convert data to ascii
			buff[2] = (value & 0x00F0)>>4;		// Filter 4 bit 3'th
			if(buff[2]<10){buff[2] |= 0x30;} else {buff[2] = (buff[2]-9) | 0x40;}	// Convert data to ascii
			buff[3] = (value & 0x000F);			// Filter 4 bit 4'th
			if(buff[3]<10){buff[3] |= 0x30;} else {buff[3] = (buff[3]-9) | 0x40;}	// Convert data to ascii		
			buff[4] = 0;	// End of data
			break;		// Out block for hexadecimal format case		
		default : 	break;		// Out block for default case		
	}
	if(buff[0]==0x30)		// if data 1'th = 0 none display
	{index = 1;}         // Shift to display 2'th
	if(buff[0]==0x30 && buff[1]==0x30)	// if data 1'th = 0 and 2'th = 0 none display
	{index = 2;}			// Shift to display 3'th
	if(buff[0]==0x30 && buff[1]==0x30 && buff[2]==0x30)	// if data 1'th = 0, 2'th = 0 and 3'th = 0 none display
	{index = 3;}			// Shift to display 4'th
	if(buff[0]==0x30 && buff[1]==0x30 && buff[2]==0x30 && buff[3]==0x30)	// if data 1'th = 0, 2'th = 0, 3'th = 0 and 4'th = 0 none display
	{index = 4;}			// Shift to display 5'th
	lcd_putstr(line_sel,&buff[index]);  	// Send integer to LCD
}
 
