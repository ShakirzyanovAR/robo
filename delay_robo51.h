/*--------------------------------------------------------------------------*/
// Program	: Wait for 100us and 1 ms per unit  
// Description 	: Wait for 100us per unit by call function delay_100us and
//		: 1 ms per unit by call function delay_ms
// Filename	: delay_robo51.h
// C compiler	: RIDE 51 V6.1
/*--------------------------------------------------------------------------*/
/*****************************************************************************/
/*********************** Delay 1 ms per unit ************************/
/*****************************************************************************/
void delay_ms(unsigned int ms)
{
	unsigned int x,a;		// Keep for counter loop
	for(x=0;x<ms;x++)
	{
		for(a=0;a<878;a++);	// Loop for delay 1 ms per unit 
	}
}
/*****************************************************************************/
/*********************** Delay 100 us per unit ********************************/
/*****************************************************************************/
void delay_100us(unsigned int us)
{
	unsigned int x,a;		// Keep for counter loop
	for(x=0;x<us;x++)
	{
		for(a=0;a<75;a++);	// Loop for delay 100 us per unit
	}
}
 
