/*--------------------------------------------------------------------------*/
// Program	: Read Analog input  
// Description 	: Read Analog input function 
// Filename	: adc.h
// C compiler	: RIDE 51 V6.1
/*--------------------------------------------------------------------------*/
int analog(char channel)
{
	int adc_dat = 0;	// For keep input analog data
	ADCF = 0x3F;   	// setup config for adc bit input convert(port1) (P1.0-P1.5)
	ADCON = 0x20;		// Enable ADC  
	ADCON &= 0xF8;		// clear SCH bit (SCH 0:1:2 = 0)
	ADCON |= channel;	// select convert/read by channel config.

	ADCON |= 0x08;    	// start convert (set bit ADSST)
	while((ADCON & 0x10)!= 0x10); // wait for EOF to set
  	ADCON &= 0xEF;	// clear bit ADEOC
  	adc_dat = (ADDH<<2)+(ADDL);	// read adc data and convert 
  	return(adc_dat);		// Return result of ADC 10 bit
}
 
