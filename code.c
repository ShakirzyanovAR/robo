#include <C51ac2.h>		// Header include register of T89C51AC2
#include <lcd_robo51.h>	// Module function LCD 
#include <dc_motor.h>	// Module function drive DC motor 
#include <sound_robo51.h>		// Module function drive sound
#include <delay_robo51.h>		// Module function delay process
#include <adc_robo51.h> // Module function read input analog
#include <ultrasonic.h>
#define pow 200    		// Define constant power drive robot 
#define ref 400			// Define constant reference compare track line
void run_fd(int time_spin){
	motor_fd(2,pow);		// Motor channel 2 forward
	motor_fd(1,pow);		// Motor channel 1 forward
	delay_ms(time_spin); // Delay time for robot drive forward
}
void run_bk(int time_spin){
	motor_bk(2,pow);		// Motor channel 2 backward
	motor_bk(1,pow);		// Motor channel 1 backward
	delay_ms(time_spin);	// Delay time for robot drive backward
}
void turn_left(int time_spin){
	motor_fd(2,pow);		// Motor channel 2 forward
	motor_bk(1,pow);		// Motor channel 1 backward
	delay_ms(time_spin);	// Delay time for robot spin turn left
}
void turn_right(int time_spin){
	motor_bk(2,pow);		// Motor channel 2 backward
	motor_fd(1,pow);		// Motor channel 1 forward
	delay_ms(time_spin);	// Delay time for robot spin turn right
} 

void main(void){
	int left=0,right=0;	// Define variable for keep input analog value
	int val=1000;
	trigger_pulse();
	lcd_init();				// Initial LCD module
	beep();  				// Sound beep 1 time
	while(1)		// Looping
	{
	   left = analog(5);	// Read input analog channel 5(connect left sensor)
		right = analog(4);// Read input analog channel 4(connect right sensor)
		val=distance();
		
		if(val<=5){
				while(val<=5 || left>ref){
					turn_left(10);
					val=distance();
				}
		}
		// ���� ����� ������ ��� ������ ���������� �� ������ �������
		else if( (left < ref) || (right < ref) ){
			motor_stop(all);			// ������������� ������
			lcd_clear();				// ������� �������
			
			// ���� ����� ������ ���������� �� ������ �������, ...
			if(left < ref)	
			{
				// ... �� ������������ �� ����
				turn_left(90); 	
			} 
			// ���� ������ ������ ���������� �� ������ �������, ...
			if(right < ref)
			{
				// ... �� ������������ �� �����
				turn_right(90);	
			}
			
			// ����� �������� �� ���� ������
			run_fd(75);
			 
		}
		else {
			run_fd(10);		// Drive robot forward
		}
					
	}
	
}
