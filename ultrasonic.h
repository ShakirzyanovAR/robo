/*--------------------------------------------------------------------------*/
// Program		: Module function read distance from SFR04 sensor 
// Description : Call function control LCD display group type 4 bit  
// Filename		: ultrasonic.h
// C compiler	: RIDE 51 V6.1
/*--------------------------------------------------------------------------*/
#include <intrins.h>		// Include library for nop function 
sbit echo = P0^0;			// Define receive pulse pin
sbit trigger = P0^1;		// Define trigger pulse pin
/*****************************************************************************/
/***************** Function Trigger pulse for start process ******************/
/*****************************************************************************/
void trigger_pulse(void)	  
{
	unsigned char i;		// Variable for counter
	trigger = 1;         // Start positive pulse
	for(i=0;i<10;i++)    // For loop 10 time
	_nop_();             // Delay 1 us function
	trigger = 0;         // End of positive pulse 	
}
/*****************************************************************************/
/************************* Function Read distance ****************************/
/*****************************************************************************/
unsigned int distance()
{
	unsigned int mc,dat,i;	// Variable for internal this function 
   trigger = 0; 				// Initial logic low
	echo = 1;					// Initial logic low
	TMOD &=0x0F;         	// Configuration timer1 mode2(16 bit counter)  
	TMOD |=0x10;
	TH1 = 0x00;            	// Initial timer1 counter value at zero
	TL1 = 0x00;
	TF1 = 0;            	   // Clear overflow flag
	TR1 = 0;               	// Start Timer1
	for(i=0;i<5;i++)  		// For loop 5 time for average data 
   	{   
		trigger_pulse();		// Send trigger pulse signal	
		while(!echo);			// Detect rising pulse
		TR1 = 1;      			// Start timer count
		while(echo); 			// Detect falling pulse
		TR1 = 0;					// Stop timer
		TF1 = 0;       		// Clear bit overflow flag
		mc = TH1;      		// Keep high byte  
		mc <<= 8;				// Shift to high byte
		mc += TL1;				// Keep low byte
		TH1 = 0x00;    		// Initial timer1 counter value at zero		
		TL1 = 0x00;
		dat = dat + (mc/5);	// Keep positive pulse length 
		delay_ms(10);			// Delay 10 ms
	}	
	return(dat/114);			// Return distance cm scale

}
 
