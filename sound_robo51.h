/*--------------------------------------------------------------------------*/
// Program	: Generate sound by frequency  
// Description 	: Generate sound function 
// Filename	: sound_robo51.h
// C compiler	: RIDE 51 V6.1
/*--------------------------------------------------------------------------*/
sbit s_bit = P2^7;
#pragma DISABLE		// for disable all interrupt before call function delay_sound
void delay_sound(unsigned int ms)  
{
	unsigned int x,a;			// Keep for counter loop
	for(x=0;x<ms;x++)
	{
		for(a=0;a<75;a++);	// Loop for delay 100 us per unit 
	}
}

#pragma DISABLE		// for disable all interrupt before call function sound,beep
void sound(int freq,int time)
{
	int dt=0,m=0;			// Keep value and keep active logic delay time
	dt = 5000/freq;   		
	time = (5*time)/dt;	// Keep counter for generating sound 
	for(m=0;m<time;m++)    
	{
		s_bit = 1;        // P2.7 = high
		delay_sound(dt);	// Delay for sound	
		s_bit = 0;	// P2.7 = low
		delay_sound(dt);	// Delay for sound
	}		
}
#pragma DISABLE		// For disable all interrupt before call function beep
void beep(void)
{
	sound(500,100);		// Generate sound at default frequency
}
 
